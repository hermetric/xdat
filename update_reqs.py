import ast

def extract_install_requires(file_path):
    with open(file_path, "r") as file:
        tree = ast.parse(file.read(), filename=file_path)
        for node in ast.walk(tree):
            if isinstance(node, ast.Call) and getattr(node.func, "id", "") == "setup":
                for keyword in node.keywords:
                    if keyword.arg == "install_requires":
                        return [elt.s for elt in keyword.value.elts]

dependencies = extract_install_requires("setup.py")
if dependencies:
    with open("requirements.txt", "w") as req_file:
        req_file.write("\n".join(dependencies))
