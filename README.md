## What is *xdat*?
xdat (eXtended Data Analysis Toolkit), is a set of utilities that help streamline data science projects.

+ Monkey-patches common packages to add useful functionality
+ Helps with standardization of plots in projects
+ Includes useful tools (like caching)


### Warning: this isn't even in "alpha" yet, and will likely have changes that are not backwards-compatible.

## Installation

### Ubuntu 20.04
```
> pip install xdat
```

Optionally, can also install:
```
> sudo apt -q -y install libeigen3-dev
> pip install git+https://github.com/madrury/py-glm.git
```


